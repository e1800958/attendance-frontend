import React from 'react';
import ReactTable from "react-table-v6";
import "react-table-v6/react-table.css";
import {useState, useEffect} from "react";

const Attendance=props=>{
    const [attendance, setAttendance]=useState([]);
    useEffect(( )=>{
        fetch("/attendances")
        .then(function(response){
            response.json().then(function(data){
                setAttendance(data); 
            })
        })
    }, []);

      const columns = [{
        Header: 'Id',
        accessor: 'id' // String-based value accessors!
      }, {
        Header: 'Key',
        accessor: 'key',
      }, {
        Header: 'Date',
        accessor: 'day',
      }]
    return (
        <div>
            <h4> Hello {props.title}</h4>
            <ReactTable data={attendance} columns={columns}/>
        </div>
    );
};

export default Attendance;